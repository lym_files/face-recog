

function toggleNavbar() {
    const navbarLinks = document.getElementById('navbar-links');
    navbarLinks.style.display = navbarLinks.style.display === 'none' ? 'flex' : 'none';
}

// Modal
var modal = document.getElementById('modal');
//var loginLink = document.getElementById('loginLink');
var signupLink = document.getElementById('signupLink');
/*
loginLink.addEventListener('click', function() {
    modal.style.display = 'block';
});
*/
signupLink.addEventListener('click', function() {
    modal.style.display = 'block';
});

modal.addEventListener('click', function(event) {
    if (event.target.classList.contains('close')) {
        modal.style.display = 'none';
    }
});

/*
function openCvReady() {
    document.addEventListener('DOMContentLoaded', () => {
    cv['onRuntimeInitialized'] = () => {
        let video = document.getElementById("cam_input"); // video is the id of video tag
        navigator.mediaDevices.getUserMedia({video: true, audio: false})
            .then(function (stream) {
                video.srcObject = stream;
                video.play();
            })
            .catch(function (err) {
                console.log("An error occurred! " + err);
            });
        let src = new cv.Mat(video.height, video.width, cv.CV_8UC4);
        let dst = new cv.Mat(video.height, video.width, cv.CV_8UC1);
        let gray = new cv.Mat();
        let cap = new cv.VideoCapture(cam_input);
        let faces = new cv.RectVector();
        let classifier = new cv.CascadeClassifier();
        let utils = new Utils('errorMessage');
        let faceCascadeFile = "haarcascade_frontalface_default.xml"; // path to xml
        let faceCascadeFileUrl = "static/JScript/haarcascade_frontalface_default.xml"; // path to xml

        utils.createFileFromUrl(faceCascadeFile, faceCascadeFileUrl, () => {
            console.log('cascade ready to load.');

            if(!classifier.load(faceCascadeFile))
            {
                console.log("cascade not loaded");
            }else{
                console.log("cascade loaded");
            }
        });
        const FPS = 50;

        function processVideo() {
            let begin = Date.now();
            cap.read(src);
            src.copyTo(dst);
            cv.cvtColor(dst, gray, cv.COLOR_RGBA2GRAY, 0);
            try {
                classifier.detectMultiScale(gray, faces, 1.1, 3, 0);
                console.log(faces.size());
            } catch (err) {
                console.log(err);
            }
            for (let i = 0; i < faces.size(); ++i) {
                let face = faces.get(i);
                let point1 = new cv.Point(face.x, face.y);
                let point2 = new cv.Point(face.x + face.width, face.y + face.height);
                cv.rectangle(dst, point1, point2, [255, 0, 0, 255]);
// crop the value (images)
            }
            cv.imshow("canvas_output", dst);
// schedule next one.
            let delay = 1000 / FPS - (Date.now() - begin);
            setTimeout(processVideo, delay);
        }

// schedule first one.
        setTimeout(processVideo, 0);
    };
    });
}

*/
