const video = document.getElementById("video");
const videoContainer = document.getElementById("video-container");
const MODEL_URI = "/static/models";
Promise.all([
  faceapi.nets.tinyFaceDetector.loadFromUri(MODEL_URI),
  faceapi.nets.faceLandmark68Net.loadFromUri(MODEL_URI),
  faceapi.nets.faceRecognitionNet.loadFromUri(MODEL_URI),
  faceapi.nets.faceExpressionNet.loadFromUri(MODEL_URI),
  faceapi.nets.ageGenderNet.loadFromUri(MODEL_URI),
])
  .then(playVideo)
  .catch((err) => {
    console.log(err);
  });

function playVideo() {
  if (!navigator.mediaDevices) {
    console.error("mediaDevices not supported");
    return;
  }
  navigator.mediaDevices
    .getUserMedia({
      video: {
        width: { min: 640, ideal: 1280, max: 1920 },
        height: { min: 360, ideal: 720, max: 1080 },
      },
      audio: false,
    })
    .then(function (stream) {
      video.srcObject = stream;
    })
    .catch(function (err) {
      console.log(err);
    });
}
let detectedFaceData = null;

video.addEventListener("play", () => {
  // Creating the canvas
  const canvas = faceapi.createCanvasFromMedia(video);

  // This will force the use of a software (instead of hardware accelerated)
  // Enable only for low configurations
  canvas.willReadFrequently = true;
  videoContainer.appendChild(canvas);

  // Resizing the canvas to cover the video element
  const canvasSize = { width: video.width, height: video.height };
  faceapi.matchDimensions(canvas, canvasSize);

  setInterval(async () => {
    const detections = await faceapi
      .detectAllFaces(video, new faceapi.TinyFaceDetectorOptions())
      .withFaceLandmarks()
      .withFaceExpressions()
      .withAgeAndGender();

    // Set detections size to the canvas size
    const DetectionsArray = faceapi.resizeResults(detections, canvasSize);
    canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);
    detectionsDraw(canvas, DetectionsArray);

    if (detections.length > 0) {
      // Enable the download button
      const downloadBtn = document.getElementById("downloadBtn");
      downloadBtn.disabled = false;

      // Store the detected face data
      detectedFaceData = detections[0];
    } else {
      // Disable the download button if no face is detected
      const downloadBtn = document.getElementById("downloadBtn");
      downloadBtn.disabled = true;
    }

  }, 50);

});

// Drawing our detections above the video
function detectionsDraw(canvas, DetectionsArray) {

  faceapi.draw.drawDetections(canvas, DetectionsArray);
  //faceapi.draw.drawFaceLandmarks(canvas, DetectionsArray);
  //faceapi.draw.drawFaceExpressions(canvas, DetectionsArray);

  // Drawing AGE and GENDER
  DetectionsArray.forEach((detection) => {
    const box = detection.detection.box;
    const drawBox = new faceapi.draw.DrawBox(box, {
      label: `${Math.round(detection.age)}y, ${detection.gender}`,
    });
    drawBox.draw(canvas);
  });
}

function downloadCroppedImage() {
  if (!detectedFaceData) {
    return;
  }

  const box = detectedFaceData.detection.box;

  const canvas = document.createElement("canvas");
  canvas.width = box.width;
  canvas.height = box.height;
  const ctx = canvas.getContext("2d");

  ctx.drawImage(
      video,
      box.x,
      box.y,
      box.width,
      box.height,
      0,
      0,
      box.width,
      box.height
  );

  const dataURL = canvas.toDataURL("image/png");

  canvas.toBlob((blob) => {
    const xhr = new XMLHttpRequest();

    xhr.open('POST', '/upload', true);
    xhr.setRequestHeader('Content-Type', 'image/png');
    xhr.onreadystatechange = function() {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        if (xhr.status === 200) {
          console.log('Image uploaded successfully!');
        } else {
          console.error('Image upload failed.');
        }
      }
    };

    xhr.send(blob);
  }, "image/png");


  const downloadLink = document.createElement("a");
  downloadLink.href = dataURL;
  downloadLink.download = "crop_face.png";

  downloadLink.click();
}

const downloadBtn = document.getElementById("downloadBtn");
downloadBtn.addEventListener("click", downloadCroppedImage);