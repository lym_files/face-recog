
let startButton = document.getElementById("startButton"); // startButton is the id of the trigger button
let isProcessing = false; // Flag to track if processing is in progress
let classifierReady = false; // Flag to track if the classifier is ready
startButton.addEventListener("click", startProcessing); // Add event listener to the button

function startProcessing() {
    if (!isProcessing) {
        isProcessing = true;
        initializeOpenCv();
    } else {
        alert("still running")
    }
}

function initializeOpenCv() {
    let video = document.getElementById("cam_input"); // video is the id of the video tag
    navigator.mediaDevices.getUserMedia({ video: true, audio: false })
        .then(function(stream) {
            video.srcObject = stream;
            video.play();
            loadClassifier();
        })
        .catch(function(err) {
            console.log("An error occurred while accessing the camera: ", err);
            isProcessing = false;
        });

    cv.onRuntimeInitialized = function() {
        console.log('OpenCV initialized.');
    };
}

function loadClassifier() {
    let utils = new Utils('errorMessage');
    let faceCascadeFile = "haarcascade_frontalface_default.xml"; // path to xml
    let faceCascadeFileUrl = "static/JScript/haarcascade_frontalface_default.xml"; // path to xml

    utils.createFileFromUrl(faceCascadeFile, faceCascadeFileUrl, () => {
        console.log('Cascade ready to load.');

        let classifier = new cv.CascadeClassifier();
        if (!classifier.load(faceCascadeFile)) {
            console.log("Cascade not loaded");
            isProcessing = false;
        } else {
            console.log("Cascade loaded");
            classifierReady = true;
            processVideo(classifier);
        }
    });
}

function processVideo(classifier) {
    if (!isProcessing) {
        return; // Stop processing if flag is false
    }

    let video = document.getElementById("cam_input"); // video is the id of the video tag
    let src = new cv.Mat(video.height, video.width, cv.CV_8UC4);
    let dst = new cv.Mat(video.height, video.width, cv.CV_8UC1);
    //let crop = new cv.Mat(video.height, video.width, cv.CV_8UC1);
    let gray = new cv.Mat();
    let cap = new cv.VideoCapture("cam_input");
    let faces = new cv.RectVector();

    const FPS = 50;

    let begin = Date.now();
    cap.read(src);
    src.copyTo(dst);
    cv.cvtColor(dst, gray, cv.COLOR_RGBA2GRAY, 0);

  //  dst.copyTo(crop);
   // cv.cvtColor(crop, gray, cv.COLOR_RGBA2GRAY, 0);

    try {
        if (classifierReady) {
            classifier.detectMultiScale(gray, faces, 1.1, 3, 0);
            console.log("Detected Face/s: ", faces.size());
        }
    } catch (err) {
        console.log("An error occurred while detecting faces: ", err);
    }
    for (let i = 0; i < faces.size(); ++i) {
        let face = faces.get(i);
        let point1 = new cv.Point(face.x, face.y);
        let point2 = new cv.Point(face.x + face.width, face.y + face.height);
        cv.rectangle(dst, point1, point2, [255, 0, 0, 255]);

        const tempCanvas = document.createElement('canvas');
        tempCanvas.width = face.width;
        tempCanvas.height = face.height;
        const croppedDataURL = tempCanvas.toDataURL();
        console.log(croppedDataURL);

    }

    cv.imshow("canvas_output", dst)
  //  cv.imshow("canvas_crop", crop)


    // Check if any faces are detected
    if (faces.size() > 0) {
        // Stop the video by pausing it
        video.pause();


        let xhr = new XMLHttpRequest();
        xhr.open("POST", "/faceimage", true);
        xhr.setRequestHeader("Content-Type", "application/json");

        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    let response = JSON.parse(xhr.responseText);
                    console.log("Received response from backend:", response);
                    console.log("Detected: ", response.faceDetected);

                    // Resume video playback if faces were not detected
                    if (!response.faceDetected) {
                        video.play();
                        setTimeout(() => {
                            processVideo(classifier);
                        }, 1000); // Wait for 1 second before checking again
                    } else {
                        //video.play();
                        isProcessing = false;
                    }
                } else {
                    console.log("Error: " + xhr.status);
                    isProcessing = false;
                }
            }
        };

        xhr.send(JSON.stringify(faces));
        isProcessing = true;
        console.log("Faces Backend: ", faces);

    } else {
        let delay = 1000 / FPS - (Date.now() - begin);
        setTimeout(() => processVideo(classifier), delay);
    }
}
