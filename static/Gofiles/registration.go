package controller

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
)

func RegistrationHandler(w http.ResponseWriter, r *http.Request) {
	/*
		db := getSQLDB()

		insertQuery := "INSERT TO user_profile(profile_avatar) VALUES ?"
		_, err := db.Exec(insertQuery, wait)
		if err != nil {
			log.Println("Insert error: ", err)
			http.Error(w, "Failed to insert user", http.StatusInternalServerError)
			return
		}
	*/
	tmpl, err := template.ParseFiles("Html/registrationPage.html")

	if err != nil {
		log.Println("Failed to parse template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, nil)
	if err != nil {
		log.Println("Failed to execute template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

func HandleFaceRegistration(w http.ResponseWriter, r *http.Request) {

	tmpl, err := template.ParseFiles("Html/faceRegistration.html")

	if err != nil {
		log.Println("Failed to parse template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, nil)
	if err != nil {
		log.Println("Failed to execute template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

func UploadHandler(w http.ResponseWriter, r *http.Request) {
	db := getSQLiteDB()

	if r.Method != http.MethodPost {
		http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Error reading request body", http.StatusInternalServerError)
		return
	}

	defer r.Body.Close()

	insertQuery := "INSERT INTO user_profile (profile_avatar) VALUES (?)"
	_, err = db.Exec(insertQuery, body)

	if err != nil {
		log.Println("Insert error: ", err)
		http.Error(w, "Failed to insert user", http.StatusInternalServerError)
		return
	}

	fmt.Println("\nImage uploaded successfully!\n")
	w.WriteHeader(http.StatusOK) // Send a 200 OK response to the client
}
