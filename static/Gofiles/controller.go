package controller

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"html/template"
	"log"
	_ "modernc.org/sqlite"
	"net/http"
)

type User struct {
	ID         int
	UserId     int
	Name       string
	Department string
	Shift      string
	Pin        string
	QR         string
	RFID       string
	Key        string
	Fprint     string
	Retinal    string
	Voice      string
}

type Face struct {
	X      int `json:"x"`
	Y      int `json:"y"`
	Width  int `json:"width"`
	Height int `json:"height"`
}

type FaceData struct {
	Faces []Face `json:"faces"`
}

var globalSQLDB *sql.DB
var globalSQLiteDB *sql.DB

func getSQLDB() *sql.DB {
	var err error
	globalSQLDB, err = sql.Open("mysql", "laxbw01:laxbw@0213!@tcp(192.168.63.220:3306)/mpp")
	if err != nil {
		panic(err)
	}

	err = globalSQLDB.Ping()
	if err != nil {
		panic(err)
	}

	fmt.Println("\nConnected to the SQL database!\n")
	return globalSQLDB
}

func getSQLiteDB() *sql.DB {

	var err error
	globalSQLiteDB, err = sql.Open("sqlite", "PROFILE.db")
	if err != nil {
		panic(err)
	}

	err = globalSQLiteDB.Ping()
	if err != nil {
		panic(err)
	}

	return globalSQLiteDB
}

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	db := getSQLDB()

	selectQuery := "SELECT DISTINCT name " +
		"FROM attendance_user"
	rows, err := db.Query(selectQuery)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	for rows.Next() {
		var name sql.NullString
		err := rows.Scan(&name)
		if err != nil {
			panic(err.Error())
		}

		if name.Valid {
			fmt.Println("Name:", name.String)
			fmt.Println("------------")
		} else {
			fmt.Println("Name: NULL")
			fmt.Println("------------")
		}
	}

	if err := rows.Err(); err != nil {
		panic(err.Error())
	}
	tmpl, err := template.ParseFiles("Html/homepage.html")

	if err != nil {
		log.Println("Failed to parse template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, nil)
	if err != nil {
		log.Println("Failed to execute template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

func UpdateHandler(w http.ResponseWriter, r *http.Request) {
	db := getSQLDB()

	updateQuery := "UPDATE attendance_user " +
		"SET user_image = ? WHERE user_id = 128"
	_, err := db.Exec(updateQuery, "hey")

	if err != nil {
		panic(err.Error())
	}

	fmt.Println("Update completed successfully")
}

func ViewHandler(w http.ResponseWriter, r *http.Request) {
	db := getSQLDB()

	selectQuery := "SELECT DISTINCT name " +
		"FROM attendance_user"
	rows, err := db.Query(selectQuery)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	for rows.Next() {
		var name sql.NullString
		err := rows.Scan(&name)
		if err != nil {
			panic(err.Error())
		}

		if name.Valid {
			fmt.Println("Name:", name.String)
			fmt.Println("------------")
		} else {
			fmt.Println("Name: NULL")
			fmt.Println("------------")
		}

	}

	if err := rows.Err(); err != nil {
		panic(err.Error())
	}

	tmpl, err := template.ParseFiles("Html/facefinder.html")
	if err != nil {
		log.Println("Failed to parse template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, nil)
	if err != nil {
		log.Println("Failed to execute template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

func HandleFaceCoordinates(w http.ResponseWriter, r *http.Request) {
	var coordinates Face
	err := json.NewDecoder(r.Body).Decode(&coordinates)
	if err != nil {
		http.Error(w, "Failed to parse request body", http.StatusBadRequest)
		return
	}
	//fmt.Println("Received face coordinates:", coordinates)
	// save to database

	//	w.WriteHeader(http.StatusOK)
	//w.Write([]byte("Face coordinates received successfully"))
}

func HandleFaceLogin(w http.ResponseWriter, r *http.Request) {

	tmpl, err := template.ParseFiles("Html/loginFace.html")
	if err != nil {
		log.Println("Failed to parse template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, nil)
	if err != nil {
		log.Println("Failed to execute template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

// ///

func HandleImage(w http.ResponseWriter, r *http.Request) {
	// Parse the JSON data from the request body
	var data FaceData
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		http.Error(w, "Error decoding JSON data: "+err.Error(), http.StatusBadRequest)
		return
	}

	// Process the face detection data here
	fmt.Printf("Received face detection data: %v", data)

	// Simulate processing delay
	//time.Sleep(1 * time.Second)

	faceDetected := len(data.Faces) > 0

	// Prepare the response
	response := map[string]interface{}{
		"message":      "This is a response from the backend",
		"faceDetected": faceDetected,
	}

	// Encode the response as JSON
	responseJSON, err := json.Marshal(response)
	if err != nil {
		http.Error(w, "Failed to encode response", http.StatusInternalServerError)
		return
	}

	// Set the response headers
	w.Header().Set("Content-Type", "application/json")

	// Write the response
	w.Write(responseJSON)
}

////

func HomeView(w http.ResponseWriter, r *http.Request) {
	db := getSQLDB()

	selectQuery := "SELECT DISTINCT name " +
		"FROM attendance_user"
	rows, err := db.Query(selectQuery)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	for rows.Next() {
		var name sql.NullString
		err := rows.Scan(&name)
		if err != nil {
			panic(err.Error())
		}

		if name.Valid {
			fmt.Println("Name:", name.String)
			fmt.Println("------------")
		} else {
			fmt.Println("Name: NULL")
			fmt.Println("------------")
		}
	}

	if err := rows.Err(); err != nil {
		panic(err.Error())
	}
	tmpl, err := template.ParseFiles("Html/index.html")

	if err != nil {
		log.Println("Failed to parse template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, nil)
	if err != nil {
		log.Println("Failed to execute template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

func HandleFaceRecognition(w http.ResponseWriter, r *http.Request) {
	/*
		db := getSQLiteDB()

		// Assuming the query to retrieve profile_avatar from the database is implemented and returns avatars as a []string.
		avatars := getProfileAvatarsFromDB(db)

		// Convert avatars to JSON
		avatarJSON, err := json.Marshal(avatars)
		if err != nil {
			log.Println("Failed to marshal avatar data to JSON:", err)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
	*/

	// Assuming avatars are fetched from the database and converted to JSON.
	avatarJSON := `["avatar1.jpg", "avatar2.jpg", "avatar3.jpg"]`

	tmpl, err := template.ParseFiles("Html/faceRecognition.html")
	if err != nil {
		log.Println("Failed to parse template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	data := struct {
		AvatarsJSON string
	}{
		AvatarsJSON: avatarJSON,
	}

	var buf bytes.Buffer
	err = tmpl.Execute(&buf, data)
	if err != nil {
		log.Println("Failed to execute template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// Optionally, you can set the Content-Type header to indicate that the response is JSON.
	// w.Header().Set("Content-Type", "application/json")

	// Write the buffer's content to the response.
	_, err = w.Write(buf.Bytes())
	if err != nil {
		log.Println("Failed to write response:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}
