package main

import (
	controller "Facial_Recognition/static/Gofiles"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {

	r := mux.NewRouter()
	staticDir := "/static/"
	r.PathPrefix(staticDir).Handler(http.StripPrefix(staticDir, http.FileServer(http.Dir("static"))))

	r.HandleFunc("/", controller.HomeView).Methods("GET")
	r.HandleFunc("/insertimage", controller.UpdateHandler).Methods("GET")
	r.HandleFunc("/camera", controller.ViewHandler).Methods("GET")

	r.HandleFunc("/facecoordinates", controller.HandleFaceCoordinates).Methods("POST")
	r.HandleFunc("/faceimage", controller.HandleImage).Methods("POST")
	r.HandleFunc("/facelogin", controller.HandleFaceLogin).Methods("GET")
	//r.HandleFunc("/faceRegistration", controller.RegistrationHandler).Methods("GET")
	r.HandleFunc("/faceRecognition", controller.HandleFaceRecognition).Methods("GET")
	r.HandleFunc("/upload", controller.UploadHandler).Methods("POST")

	r.HandleFunc("/registration", controller.HandleFaceRegistration).Methods("GET")
	//r.HandleFunc("/compare", controller.CompareHandler).Methods("POST")

	//
	//r.HandleFunc("/", controller.HomeHandler).Methods("GET")

	http.Handle("/", r)

	fmt.Println("Server listening on port 9000...")
	http.ListenAndServe(":9000", nil)
}
